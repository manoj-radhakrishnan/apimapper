"use strict";
var Data_DateTime = require("../Data.DateTime");
var Data_Maybe = require("../Data.Maybe");

// data TxnObjectType = SUBSCRIPTION_REGISTER
//                     | SUBSCRIPTION_PAYMENT
//                     | ORDER_PAYMENT
var AUTH_AND_SETTLEREFUND = (function () {
    function AUTH_AND_SETTLEREFUND() {

    };
    AUTH_AND_SETTLEREFUND.value = new AUTH_AND_SETTLEREFUND();
    return AUTH_AND_SETTLEREFUND;
})();

// data TxnObjectType = SUBSCRIPTION_REGISTER
//                     | SUBSCRIPTION_PAYMENT
//                     | ORDER_PAYMENT
var CANCEL_AUTH = (function () {
    function CANCEL_AUTH() {

    };
    CANCEL_AUTH.value = new CANCEL_AUTH();
    return CANCEL_AUTH;
})();

// data TxnObjectType = SUBSCRIPTION_REGISTER
//                     | SUBSCRIPTION_PAYMENT
//                     | ORDER_PAYMENT
var AUTH = (function () {
    function AUTH() {

    };
    AUTH.value = new AUTH();
    return AUTH;
})();

/**
 * -[>data Gateway = AXIS(1),<]-
 */
/**
 * -[>| HDFC(2),<]-
 */
/**
 * -[>| ICICI(3),<]-
 */
/**
 * -[>| CITI(4),<]-
 */
/**
 * -[>| AMEX(5),<]-
 */
/**
 * -[>| CYBERSOURCE(6),<]-
 */
/**
 * -[>| IPG(7),<]-
 */
/**
 * -[>| MIGS(8),<]-
 */
/**
 * -[>| KOTAK(9),<]-
 */
/**
 * -[>| EBS(11),<]-
 */
/**
 * -[>| PAYU(12),<]-
 */
/**
 * -[>| CCAVENUE(13),<]-
 */
/**
 * -[>| CITRUS(14),<]-
 */
/**
 * -[>| ATOM(15),<]-
 */
/**
 * -[>| CCAVENUE_V2(16),<]-
 */
/**
 * -[>| TPSL(17),<]-
 */
/**
 * -[>| PAYTM(18),<]-
 */
/**
 * -[>| PAYPAL(20),<]-
 */
/**
 * -[>| HDFC_EBS_VAS(21),<]-
 */
/**
 * -[>| PAYLATER(22),<]-
 */
/**
 * -[>| RAZORPAY(23),<]-
 */
/**
 * -[>| FSS_ATM_PIN(24),<]-
 */
/**
 * -[>| EBS_V3(25),<]-
 */
/**
 * -[>| ZAAKPAY(26),<]-
 */
/**
 * -[>| BILLDESK(27),<]-
 */
/**
 * -[>| BLAZEPAY(29),<]-
 */
/**
 * -[>| FSS_ATM_PIN_V2(30),<]-
 */
/**
 * -[>| MOBIKWIK(31),<]-
 */
/**
 * -[>| OLAMONEY(32),<]-
 */
/**
 * -[>| FREECHARGE(33),<]-
 */
/**
 * -[>| MPESA(34),<]-
 */
/**
 * -[>| SBIBUDDY(35),<]-
 */
/**
 * -[>| JIOMONEY(36),<]-
 */
/**
 * -[>| AIRTELMONEY(37),<]-
 */
/**
 * -[>| AMAZONPAY(38),<]-
 */
/**
 * -[>| PHONEPE(39),<]-
 */
/**
 * -[>| STRIPE(50),<]-
 */
/**
 * -[>| DUMMY(100),<]-
 */
/**
 * -[>| HDFC_IVR(201),<]-
 */
/**
 * -[>| ZESTMONEY(250),<]-
 */
/**
 * -[>| AXISNB(300),<]-
 */
/**
 * -[>| TPSL_SI(400),<]-
 */
/**
 * -[>| AXIS_UPI(500),<]-
 */
/**
 * -[>| HDFC_UPI(501),<]-
 */
/**
 * -[>| INDUS_UPI(502),<]-
 */
/**
 * -[>| KOTAK_UPI(503),<]-
 */
/**
 * -[>| SBI_UPI(504),<]-
 */
/**
 * -[>| ICICI_UPI(505),<]-
 */
/**
 * -[>| VIJAYA_UPI(506),<]-
 */
/**
 * -[>| HSBC_UPI(507)<]-
 */
/**
 * -data AuthType = ATMPIN-
 */
/**
 * -| THREE_DS-
 */
/**
 * -| OTP-
 */
/**
 * -data PaymentMethodType = CONSUMER_FINANCE | UPI_METHOD-
 */
/**
 * -data CardType = CREDIT | DEBIT | PREPAID | NB | WALLET | PAYLATER | UPI | ATM_CARD-
 */
var PayuRequest = function (x) {
    return x;
};
var OrderReference = function (x) {
    return x;
};
var TxnDetail = function (x) {
    return x;
};
var CitrusRequest = function (x) {
    return x;
};
module.exports = {
    OrderReference: OrderReference,
    TxnDetail: TxnDetail,
    AUTH_AND_SETTLEREFUND: AUTH_AND_SETTLEREFUND,
    CANCEL_AUTH: CANCEL_AUTH,
    AUTH: AUTH,
    PayuRequest: PayuRequest,
    CitrusRequest: CitrusRequest
};
