"use strict";
var Control_Semigroupoid = require("../Control.Semigroupoid");
var Data_Function = require("../Data.Function");
var Data_Maybe = require("../Data.Maybe");
var Data_Record = require("../Data.Record");
var Data_Record_Builder = require("../Data.Record.Builder");
var Data_Show = require("../Data.Show");
var Data_Symbol = require("../Data.Symbol");
var Prelude = require("../Prelude");
var Type_Row = require("../Type.Row");
var Types = require("../Types");

// Note:
// 1) Below transformation functions may be kept as pure functions (for quick development cycle avoiding revisit)
// a. Keep all DB calls & remote calls outside this function
// 2) SProxy is not helping in mapping keys and transformation from excel sheet to code 
// 3) Alternative: Using Typeclass
// 4) Variations:
// a. Overall Requirements (TxnDetail, OrderReference)
// b. Keys
// c. Values
// d. Data transformation (fromDB, fromRemote, Concat of 2 or more fields, Conditional logics, )
var txnToPayURequest = function (v) {
    return function (v1) {
        return Types.PayuRequest(Data_Record_Builder.build(Control_Semigroupoid.composeFlipped(Data_Record_Builder.semigroupoidBuilder)(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "txnid";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "txnId";
        }))()(Data_Symbol.SProxy.value)(v))))(Control_Semigroupoid.composeFlipped(Data_Record_Builder.semigroupoidBuilder)(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "amount";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Show.show(Data_Maybe.showMaybe(Data_Show.showNumber))(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "txnAmount";
        }))()(Data_Symbol.SProxy.value)(v)))))(Control_Semigroupoid.composeFlipped(Data_Record_Builder.semigroupoidBuilder)(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "email";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Show.show(Data_Maybe.showMaybe(Data_Show.showString))(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "customer_email";
        }))()(Data_Symbol.SProxy.value)(v1)))))(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "phone";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Show.show(Data_Maybe.showMaybe(Data_Show.showString))(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "customer_phone";
        }))()(Data_Symbol.SProxy.value)(v1))))))))({}));
    };
};
var txnToCitrusRequest = function (v) {
    return function (v1) {
        return Types.CitrusRequest(Data_Record_Builder.build(Control_Semigroupoid.composeFlipped(Data_Record_Builder.semigroupoidBuilder)(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "merchantTxnId";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "txnId";
        }))()(Data_Symbol.SProxy.value)(v))))(Data_Record_Builder.insert()(Type_Row.rowLacks()()()(Type_Row.rowLacking))(new Data_Symbol.IsSymbol(function () {
            return "amount";
        }))(Data_Symbol.SProxy.value)(Data_Maybe.Just.create(Data_Show.show(Data_Maybe.showMaybe(Data_Show.showNumber))(Data_Record.get(new Data_Symbol.IsSymbol(function () {
            return "txnAmount";
        }))()(Data_Symbol.SProxy.value)(v))))))({}));
    };
};
module.exports = {
    txnToPayURequest: txnToPayURequest,
    txnToCitrusRequest: txnToCitrusRequest
};
