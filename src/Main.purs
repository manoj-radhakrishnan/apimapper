module Main where

import Prelude (show, ($), (>>>))
import Types
import Data.Record (get)
import Data.Record.Builder (build, insert)
import Data.Symbol (SProxy(..))
import Data.Maybe (Maybe(..))


-- Note:
-- 1) Below transformation functions may be kept as pure functions (for quick development cycle avoiding revisit)
      -- a. Keep all DB calls & remote calls outside this function
-- 2) SProxy is not helping in mapping keys and transformation from excel sheet to code 
-- 3) Alternative: Using Typeclass for mapper
-- 4) TODO: Add more details on variations among request types
            -- a. Overall Requirements (TxnDetail, OrderReference)
            -- b. Keys
            -- c. Values
            -- d. Data transformation (fromDB, fromRemote, Concat of 2 or more fields, Conditional logics, )

txnToPayURequest :: TxnDetail -> OrderReference -> PayuRequest
txnToPayURequest (TxnDetail t) (OrderReference o) = PayuRequest $
  build (
      insert (SProxy :: SProxy "txnid")      (Just $ get (SProxy :: SProxy "txnId") t)
      >>> insert (SProxy :: SProxy "amount") (Just $ show $ get (SProxy :: SProxy "txnAmount") t)
      >>> insert (SProxy :: SProxy "email")  (Just $ show $ get (SProxy :: SProxy "customer_email") o)
      >>> insert (SProxy :: SProxy "phone")  (Just $ show $ get (SProxy :: SProxy "customer_phone") o)
      ) {}

txnToPayURequest1 :: TxnDetail -> OrderReference -> PayuRequest 
txnToPayURequest1 t o = PayuRequest 
{ txnId : t.txnId
, amount : t.txnAmount
, email : o.customer_email
, phone : o.customer_phone
}     

txnToCitrusRequest :: TxnDetail -> OrderReference -> CitrusRequest
txnToCitrusRequest (TxnDetail t) (OrderReference o) = CitrusRequest $
  build (
      insert (SProxy :: SProxy "merchantTxnId")  (Just $ get (SProxy :: SProxy "txnId") t)
      >>> insert (SProxy :: SProxy "amount") (Just $ show $ get (SProxy :: SProxy "txnAmount") t)
      ) {}

