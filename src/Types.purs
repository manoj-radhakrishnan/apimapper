module Types where

import Data.DateTime (DateTime)
import Data.Maybe


newtype OrderReference = OrderReference
 { order_id :: String
 , amount :: Number
 , currency :: Maybe String
 , customer_id :: Maybe String
 , customer_email :: Maybe String
 , customer_phone :: Maybe String
 , description :: Maybe String
 , return_url :: Maybe String
 , product_id :: Maybe String
 , billing_address_first_name      :: Maybe String
 , billing_address_last_name       :: Maybe String
 , billing_address_line1           :: Maybe String
 , billing_address_line2           :: Maybe String
 , billing_address_line3           :: Maybe String
 , billing_address_city            :: Maybe String
 , billing_address_state           :: Maybe String
 , billing_address_country         :: Maybe String
 , billing_address_postal_code     :: Maybe String
 , billing_address_phone           :: Maybe String
 , billing_address_country_code_iso :: Maybe String
 , shipping_address_first_name      :: Maybe String
 , shipping_address_last_name       :: Maybe String
 , shipping_address_line1           :: Maybe String
 , shipping_address_line2           :: Maybe String
 , shipping_address_line3           :: Maybe String
 , shipping_address_city            :: Maybe String
 , shipping_address_state           :: Maybe String
 , shipping_address_country         :: Maybe String
 , shipping_address_postal_code     :: Maybe String
 , shipping_address_phone           :: Maybe String
 , shipping_address_country_code_iso :: Maybe String
 , udf1 :: Maybe String
 , udf2 :: Maybe String
 , udf3 :: Maybe String
 , udf4 :: Maybe String
 , udf5 :: Maybe String
 , udf6 :: Maybe String
 , udf7 :: Maybe String
 , udf8 :: Maybe String
 , udf9 :: Maybe String
 , udf10 :: Maybe String
 , metaData :: Maybe String
 }
newtype TxnDetail = TxnDetail
      {
       type :: TxnType,
       txnId :: String,
       txnAmount :: Maybe Number,
       txnUuid :: Maybe String,
       orderId :: String,
       merchantId :: String,
       username :: Maybe String,
       addToLocker :: Boolean,
       expressCheckout :: Maybe Boolean,
       gateway :: Maybe Gateway,
       successResponseId :: Maybe Int,
       status :: String,
       errorMessage :: Maybe String,
       isEmi :: Boolean,
       emiBank :: Maybe String,
       emiTenure :: Int,
       txnObjectType :: Maybe TxnObjectType,
       sourceObject :: Maybe String,
       sourceObjectId :: Maybe String,
       currency :: Maybe String,
       dateCreated :: DateTime,
       lastModified :: DateTime,
       bankErrorCode :: Maybe String,
       bankErrorMessage :: Maybe String,
       gatewayPayload :: Maybe String,
       redirect :: Maybe Boolean
      }


type TxnObjectType = String
-- data TxnObjectType = SUBSCRIPTION_REGISTER
--                     | SUBSCRIPTION_PAYMENT
--                     | ORDER_PAYMENT

data TxnType = AUTH_AND_SETTLEREFUND
              | CANCEL_AUTH
              | AUTH

{--data TxnMode = TEST | PROD--}

{--[>data TxnStatus = STARTED(20)<]--}
               {--[>| AUTHENTICATION_FAILED(26)<]--}
               {--[>| JUSPAY_DECLINED(22)<]--}
               {--[>| PENDING_VBV(23)<]--}
               {--[>| VBV_SUCCESSFUL(24)<]--}
               {--[>| AUTHORIZED(25)<]--}
               {--[>| AUTHORIZATION_FAILED(27)<]--}
               {--[>| CHARGED(21)<]--}
               {--[>| AUTHORIZING(28)<]--}

type Gateway = String
{--[>data Gateway = AXIS(1),<]--}
                    {--[>| HDFC(2),<]--}
                    {--[>| ICICI(3),<]--}
                    {--[>| CITI(4),<]--}
                    {--[>| AMEX(5),<]--}
                    {--[>| CYBERSOURCE(6),<]--}
                    {--[>| IPG(7),<]--}
                    {--[>| MIGS(8),<]--}
                    {--[>| KOTAK(9),<]--}
                    {--[>| EBS(11),<]--}
                    {--[>| PAYU(12),<]--}
                    {--[>| CCAVENUE(13),<]--}
                    {--[>| CITRUS(14),<]--}
                    {--[>| ATOM(15),<]--}
                    {--[>| CCAVENUE_V2(16),<]--}
                    {--[>| TPSL(17),<]--}
                    {--[>| PAYTM(18),<]--}
                    {--[>| PAYPAL(20),<]--}
                    {--[>| HDFC_EBS_VAS(21),<]--}
                    {--[>| PAYLATER(22),<]--}
                    {--[>| RAZORPAY(23),<]--}
                    {--[>| FSS_ATM_PIN(24),<]--}
                    {--[>| EBS_V3(25),<]--}
                    {--[>| ZAAKPAY(26),<]--}
                    {--[>| BILLDESK(27),<]--}
                    {--[>| BLAZEPAY(29),<]--}
                    {--[>| FSS_ATM_PIN_V2(30),<]--}
                    {--[>| MOBIKWIK(31),<]--}
                    {--[>| OLAMONEY(32),<]--}
                    {--[>| FREECHARGE(33),<]--}
                    {--[>| MPESA(34),<]--}
                    {--[>| SBIBUDDY(35),<]--}
                    {--[>| JIOMONEY(36),<]--}
                    {--[>| AIRTELMONEY(37),<]--}
                    {--[>| AMAZONPAY(38),<]--}
                    {--[>| PHONEPE(39),<]--}
                    {--[>| STRIPE(50),<]--}
                    {--[>| DUMMY(100),<]--}
                    {--[>| HDFC_IVR(201),<]--}
                    {--[>| ZESTMONEY(250),<]--}
                    {--[>| AXISNB(300),<]--}
                    {--[>| TPSL_SI(400),<]--}
                    {--[>| AXIS_UPI(500),<]--}
                    {--[>| HDFC_UPI(501),<]--}
                    {--[>| INDUS_UPI(502),<]--}
                    {--[>| KOTAK_UPI(503),<]--}
                    {--[>| SBI_UPI(504),<]--}
                    {--[>| ICICI_UPI(505),<]--}
                    {--[>| VIJAYA_UPI(506),<]--}
                    {--[>| HSBC_UPI(507)<]--}


{--data AuthType = ATMPIN--}
								{--| THREE_DS--}
                {--| OTP--}

{--data PaymentMethodType = CONSUMER_FINANCE | UPI_METHOD--}

{--data CardType = CREDIT | DEBIT | PREPAID | NB | WALLET | PAYLATER | UPI | ATM_CARD--}


newtype PayuRequest = PayuRequest {
            txnid :: Maybe String,
            amount :: Maybe String,
            email :: Maybe String,
            phone :: Maybe String
            {--firstname :: Maybe String,--}
            {--lastname :: Maybe String,--}
            {--address1 :: Maybe String,--}
            {--address2 :: Maybe String,--}
            {--city :: Maybe String,--}
            {--state :: Maybe String,--}
            {--country :: Maybe String,--}
            {--zipcode :: Maybe String,--}
            {--surl :: Maybe String,--}
            {--furl :: Maybe String,--}
            {--curl :: Maybe String,--}
            {--ccname :: Maybe String,--}
            {--ccnum :: Maybe String,--}
            {--ccvv :: Maybe String,--}
            {--ccexpyr :: Maybe String,--}
            {--ccexpmon :: Maybe String,--}
            {--store_card_token :: Maybe String,--}
            {--user_credentials :: Maybe String,--}
            {--key :: Maybe String,--}
            {--bankcode :: Maybe String,--}
            {--pg :: Maybe String,--}
            {--sodexo_sourceId :: Maybe String,--}
            {--productinfo :: Maybe String,--}
            {--hash :: Maybe String,--}
            {--udf1 :: Maybe String,--}
            {--udf2 :: Maybe String,--}
            {--udf3 :: Maybe String,--}
            {--udf4 :: Maybe String,--}
            {--udf5 :: Maybe String,--}
            {--udf6 :: Maybe String,--}
            {--udf7 :: Maybe String,--}
            {--udf8 :: Maybe String,--}
            {--udf9 :: Maybe String--}
            }

newtype CitrusRequest = CitrusRequest {
						amount :: Maybe String,
            -- returnUrl :: Maybe String,
            merchantTxnId :: Maybe String
            -- cardHolderName :: Maybe String,
            -- cardNumber :: Maybe String,
            -- cvvNumber :: Maybe String,
            -- expiryYear :: Maybe String,
            -- expiryMonth :: Maybe String,
            -- cardType :: Maybe String,
            -- email :: Maybe String,
            -- mobile :: Maybe String,
            -- firstName :: Maybe String,
            -- lastName :: Maybe String,
            -- address :: Maybe String,
            -- addressCity :: Maybe String,
            -- addressState :: Maybe String,
            -- addressZip :: Maybe String,
            -- issuerCode :: Maybe String
						}

